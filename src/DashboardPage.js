import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from '@reach/router';
import Button from '@material-ui/core/Button';
import Main from '@openpatch/ui-core/lib/Main';

import UndrawBuildingBlocks from './UndrawBuildingBlocks';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '100%',
    width: '100%'
  }
}));

function DashboardPage() {
  const classes = useStyles();

  return (
    <Main className={classes.root}>
      <UndrawBuildingBlocks />
      <Button color="primary" component={Link} to="/itembank/create-item">
        Start Building
      </Button>
    </Main>
  );
}

DashboardPage.propTypes = {};

export default DashboardPage;
