import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import coreReducer from '@openpatch/ui-core/lib/redux/reducer';
import authentificationReducer from '@openpatch/ui-authentification/lib/redux/reducer';
import itembankReducer from '@openpatch/ui-itembank/lib/redux/reducer';

const rootReducer = combineReducers({
  core: coreReducer,
  authentification: authentificationReducer,
  itembank: itembankReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = applyMiddleware(thunk);

export default createStore(
  rootReducer,
  undefined,
  composeEnhancers(middlewares)
);
