import React from 'react';
import ReactDOM from 'react-dom';
import * as Sentry from '@sentry/browser';
import App from './App';
import * as serviceWorker from './serviceWorker';

import store from './redux/store';
import { authApi } from '@openpatch/ui-authentification/lib/api';
import itembankApi from '@openpatch/ui-itembank/lib/api';

// configure microservice endpoints
if (process.env.NODE_ENV !== 'production') {
  authApi.defaults.baseURL = 'http://localhost:5000';
  authApi.defaults.authBaseURL = 'http://localhost:5000';

  itembankApi.defaults.baseURL = 'http://localhost:5001';
  itembankApi.defaults.authBaseURL = 'http://localhost:5000';
}

authApi.defaults.store = store;
itembankApi.defaults.store = store;

Sentry.init({
  dsn: 'https://b0af536fca11446b95a1c2c0a335588f@sentry.io/1493558',
  environment: process.env.NODE_ENV
});

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
