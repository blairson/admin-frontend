import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Loadable from 'react-loadable';
import { Router } from '@openpatch/ui-core/lib/router';
import ThemeProvider from '@openpatch/ui-core/lib/ThemeProvider';
import I18nProvider from '@openpatch/ui-core/lib/I18nProvider';
import NotificationsProvider from '@openpatch/ui-core/lib/NotificationsProvider';
import NotFound from '@openpatch/ui-core/lib/NotFound';
import uiCoreLocale from '@openpatch/ui-core/lib/locale';
import authentificationLocale from '@openpatch/ui-authentification/lib/locale';
import itembankLocale from '@openpatch/ui-itembank/lib/locale';

import AppShell from './AppShell';
import SentryError from './SentryError';
import store from './redux/store';

const AsyncDashboard = Loadable({
  loader: () => import('./DashboardPage'),
  loading: () => 'Loading...'
});

const AsyncAuthentification = Loadable({
  loader: () => import('@openpatch/ui-authentification/lib/AdminApp'),
  loading: () => 'Loading...'
});

const AsyncItembank = Loadable({
  loader: () => import('@openpatch/ui-itembank/lib/AdminApp'),
  loading: () => 'Loading...'
});

class App extends Component {
  render() {
    return (
      <SentryError>
        <Provider store={store}>
          <I18nProvider
            locales={[uiCoreLocale, authentificationLocale, itembankLocale]}
          >
            <ThemeProvider>
              <React.Fragment>
                <NotificationsProvider />
                <AppShell>
                  <Router>
                    <AsyncDashboard path="/" />
                    <AsyncAuthentification path="authentification/*" />
                    <AsyncItembank path="itembank/*" />
                    <NotFound default />
                  </Router>
                </AppShell>
              </React.Fragment>
            </ThemeProvider>
          </I18nProvider>
        </Provider>
      </SentryError>
    );
  }
}

export default App;
