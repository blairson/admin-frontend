import React, { Component } from 'react';
import { Link } from '@reach/router';
import PropTypes from 'prop-types';

class SectionLink extends Component {
  static propTypes = {
    href: PropTypes.string.isRequired
  };

  render() {
    const { href, ...props } = this.props;
    return <Link to={href} {...props} />;
  }
}

export default SectionLink;
