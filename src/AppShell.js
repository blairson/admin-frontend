import React from 'react';
import {
  navigation as authentificationNavigation,
  widgets as authentificationWidgets
} from '@openpatch/ui-authentification/lib/AdminApp';
import { navigation as itembankNavigation } from '@openpatch/ui-itembank/lib/AdminApp';
import withAuth from '@openpatch/ui-authentification/lib/withAuth';
import DrawerLayout from '@openpatch/ui-core/lib/DrawerLayout';

import SectionLink from './SectionLink';

export class AppShell extends React.Component {
  convertToSection = (baseUrl, navigation) => {
    const {
      claims: { role }
    } = this.props;

    const links = [];

    navigation.links.forEach(link => {
      const href = baseUrl + link.to;
      const label = link.label;
      const icon = link.icon;

      if (!link.allowedRoles || link.allowedRoles.includes(role)) {
        links.push({
          href,
          label,
          icon
        });
      }
    });

    const section = {
      label: navigation.title,
      links,
      component: SectionLink
    };
    return section;
  };

  render() {
    const appBar = {
      right: (
        <div>
          <authentificationWidgets.AvatarMenu />
        </div>
      )
    };

    const sections = [];
    sections.push({
      links: [
        {
          href: '/',
          label: 'Dashboard',
          icon: 'dashboard'
        }
      ],
      component: SectionLink
    });
    sections.push(
      this.convertToSection('/authentification', authentificationNavigation),
      this.convertToSection('/itembank', itembankNavigation)
    );

    const footer = {
      copyright: `© ${new Date().getFullYear()} Openpatch`
    };

    const drawer = { sections, footer, variant: 'temporary' };

    return (
      <DrawerLayout
        appBar={appBar}
        drawer={drawer}
        children={this.props.children}
      />
    );
  }
}

export default withAuth(['admin', 'editor', 'analyst'])(AppShell);
