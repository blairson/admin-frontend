FROM registry.gitlab.com/openpatch/react-microservice-base:v1.1.0 as build

COPY yarn.lock /yarn.lock
COPY package.json /package.json

COPY . /usr/src/app

RUN yarn install
RUN yarn build

FROM nginx:1.17-alpine
COPY --from=build /usr/src/app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
